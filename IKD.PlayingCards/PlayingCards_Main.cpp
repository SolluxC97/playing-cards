// Lab Excerise 2
// Playing Cards
// Izaac "Sollux" Dewilde

#include <iostream>
#include <conio.h>
#include <string>

enum Rank
{
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, HEART, CLUB, DIAMOND
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{

	_getch();
	return 0;
}